#include "Agents.hpp"

int randInt(int min, int max)
{
    /* Return a random int from min to max.
     * Notice that the result may include both min and max.
     * i.e. return a number in [min, max].
     */
    if (max < min)
    {
        int temp = max;
        max = min;
        min = temp;
    }
    return rand() % (max - min + 1) + min;
}


Agent::Agent()
{
    setName("Virtual Agent");
}

Agent::~Agent()
{
}

void
Agent::setName(char const *name)
{
    this->m_name = name;
}

char const *
Agent::getName()
{
    return this->m_name;
}


RandomAgent::RandomAgent()
{
    setName("Random Agent");
}

RandomAgent::~RandomAgent()
{
}

char
RandomAgent::getAction(State gameState)
{
    std::vector<char> actions = gameState.getActions();
    int index = randInt(0, actions.size()-1);
    return actions[index];

}


MonteCarloAgent::MonteCarloAgent()
{
    m_trial_time = 100;
    setName("Monte Carlo Agent");
}

MonteCarloAgent::~MonteCarloAgent()
{
}

void
MonteCarloAgent::setTrialTime(int trial_time)
{
    this->m_trial_time = trial_time;
}

int
MonteCarloAgent::getTrialTime()
{
    return this->m_trial_time;
}


int
MonteCarloAgent::randomlyPlayGameOnce(State gameState)
{
    RandomAgent agent;
    int total_score = 0;
    State state = gameState;

    while (!state.isDeadState())
    {
        char action = agent.getAction(state);
        int step_score = 0;
        state = state.getSuccessor(action, &step_score);
        total_score += step_score;
        state = state.getAddTileSuccessor();
        
    }

    return total_score;
}

double
MonteCarloAgent::randomlyPlayGameManyTimes(State gameState, bool nextIsAgent)
{
    int TRIAL_TIME = getTrialTime();
    long total_score = 0;

    if (nextIsAgent)
    {
        for (int j = 0; j < TRIAL_TIME; j++)
            total_score += randomlyPlayGameOnce(gameState);
    } else
    {
        for (int j = 0; j < TRIAL_TIME; j++)
        {
            State nextState = gameState.getAddTileSuccessor();
            total_score += randomlyPlayGameOnce(nextState);
        }
    }
    
    return total_score / TRIAL_TIME;
}


char
MonteCarloAgent::getAction(State gameState)
{
    std::vector<char> actions = gameState.getActions();
    std::vector<double> scores;

    for (int i = 0; i < actions.size(); i++)
    {
        int action_score = 0;

        State nextState = gameState.getSuccessor(actions[i], &action_score);
        
        scores.push_back( (double)action_score + randomlyPlayGameManyTimes(nextState, false) );
        
    }

    double max_score = 0;
    int max_index = 0;
    for (int i = 0; i < scores.size(); i++)
    {
        if (scores[i] > max_score)
        {
            max_score = scores[i];
            max_index = i;
        }
            
    }
    
    return actions[max_index];
    
}


ConcurrentMonteCarloAgent::ConcurrentMonteCarloAgent()
{
    setName("Concurrent Monte Carlo Agent");
}

ConcurrentMonteCarloAgent::~ConcurrentMonteCarloAgent()
{
}

/* *********** TASK 2  Concurrent Monte Carlo Agent ************ *
 * Use the concurrency method to imporve your Monte Carlo Agent! *
 *                                                               *
 * Implement [ char ConcurrentMonteCarloAgent::getAction(State   *
 * gameState) ]                                                  *
 *                                                               *
 * You will be happy to find that it runs much faster :)         *
 * ************************************************************* */

char
ConcurrentMonteCarloAgent::getAction(State gameState)
{
    /* Get an action given a game state.
     * For Concurrent Monte Carlo Agent, you should improve the implementation by
     * using concurrency to simulate the random game playing after different actions 
     * simultaneously. i.e. For each action, get a new thread to simulate the game.
     * There should be at most actions.size() threads in your implementation.
     * 
     * There is a new method 
     * [ double MonteCarloAgent::randomlyPlayGameManyTimes(State gameState, bool nextIsAgent) ]
     * to help you reduce the workload. You can check it above. It is important for
     * next task too, so just use it.
     * 
     * HINT: Just modify the code in MonteCarloAgent::getAction.
     * HINT: You can use std::thread or std::async, both are fine. Remember to add
     * the header file in Agents.hpp!
     */
    
    // YOUR CODE HERE

    // Delete these comments and next line after your implementation.
    // This is just to avoid warning before implementation.
    return 'A';
    
}

FurtherConcurrentMonteCarloAgent::FurtherConcurrentMonteCarloAgent()
{
    setName("FurtherConcurrent Monte Carlo Agent");
}

FurtherConcurrentMonteCarloAgent::~FurtherConcurrentMonteCarloAgent()
{
}

/* ***************** TASK 3  Further More ... ****************** *
 * Our agent is still not satisfied! He wants to get faster, so  *
 * he asks you to use concurrency to imporve the function        *
 * randomlyPlayGameManyTimes too!                                *
 *                                                               *
 * Implement [ double                                            *
 * FurtherConcurrentMonteCarloAgent::randomlyPlayGameManyTimes   *
 * (State gameState, bool nextIsAgent) ]                         *
 *                                                               *
 * Does it really run faster?                                    *
 * ************************************************************* */

double
FurtherConcurrentMonteCarloAgent::randomlyPlayGameManyTimes(State gameState, bool nextIsAgent)
{
    /* The functionality should be the same as MonteCarloAgent::randomlyPlayGameManyTimes.
     * But use concurrency to play EACH game simultaneously.
     * 
     * HINT: Just modify the code in MonteCarloAgent::randomlyPlayGameManyTimes.
     */
    
    // YOUR CODE HERE

    // Delete these comments and next line after your implementation.
    // This is just to avoid warning before implementation.
    return 0.0;

}