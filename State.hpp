#ifndef STATE_HPP_
#define STATE_HPP_

#include <iostream>
#include <vector>
#include <random>
#include <list>
#include "platform.hpp"

class State
{
private:
    int m_chess[4][4];

public:
    State(chess_type chess);
    State(Game game);
    ~State();

    chess_type getChessBoard();
    std::vector<char> getActions();
    State getSuccessor(char action);
    State getSuccessor(char action, int *score);
    State getAddTileSuccessor();
    std::list<std::pair<State, double>> getAddTileSuccessorList();
    bool isDeadState();
    int getSpace();

};

#endif